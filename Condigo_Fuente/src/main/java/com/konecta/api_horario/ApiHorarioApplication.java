package com.konecta.api_horario;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@SpringBootApplication
public class ApiHorarioApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ApiHorarioApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ApiHorarioApplication.class, args);
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				
				
				registry.addMapping("/").allowedOrigins("*").allowedMethods("GET");
				registry.addMapping("/hora").allowedOrigins(
						"https://www.vivaair.com/",
						"https://www.vivaair.com/#/co/es/",
						"https://www.vivaair.com/#/co/es",
						"https://www.vivaair.com/#/co/en/",
						"https://www.vivaair.com/#/co/en",
						"https://www.vivaair.com/#/pe/es",
						"https://www.vivaair.com/#/pe/en",
						"https://asistenciawebv2.grupokonecta.co/",
						"https://asistenciawebv2.grupokonecta.co:8443/",
						"https://asistenciawebv2.grupokonecta.co:8443",
						"https://asistenciawebv2.grupokonecta.co",
						"https://asistenciawebv2-dev.grupokonecta.co:5005/",
						"https://asistenciawebv2.grupokonecta.co:5005/",
						"https://asistenciawebv2-dev.grupokonecta.co:5005",
						"https://asistenciawebv2.grupokonecta.co:5005",
						"https://asistenciawebv2-dev.grupokonecta.local/",
						"https://asistenciawebv2-dev.grupokonecta.co/",
						"https://asistenciawebv2.grupokonecta.local/",
						"https://asistencia.webv2.allus.com.co/WebAPI802/",
						"https://www.fna.gov.co/",
						"https://www.fna.gov.co",
						"https://www.eltiempo.com/",
	                    "https://www.eltiempo.com",
	                    "https://www.clubvivamos.com/",
	                    "https://eltiempo.zendesk.com/",
	                    "https://www.clubvivamos.com",
	                    "https://eltiempo.zendesk.com",
	                    "https://tienda.chocolatecorona.com",
						"https://capsulasexpressdev.myvtex.com",
						"https://www.capsulasexpressnutresa.com",
						"https://ws.grupokonecta.co:5001"
						)
						.allowedHeaders("*")
						.allowedMethods("POST");
			}
		};
	}
	
}